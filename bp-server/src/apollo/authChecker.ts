// @ts-ignore
import { AuthChecker } from 'type-graphql'
import { Container } from 'typedi'
import { ObjectId } from 'mongodb'
import { Role } from '../models/user'
import { UserRepository } from '../repositories/user.repository'
import { Context } from '../apollo/context.apollo'

export const authChecker: AuthChecker<Context, Role> = async (
  { context },
  role
) => {
  const { user } = context

  if (role.length === 0) {
    return user !== undefined
  }

  if (!user) {
    return false
  }
  const userService = Container.get(UserRepository)
  const userProfile = await userService.findOne({ _id: new ObjectId(user.id) })
  if (!userProfile) {
    return false
  }

  if (role.includes(userProfile.role)) {
    return true
  }

  // no roles matched, restrict access
  return false
}
