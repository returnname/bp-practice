import { ContextFunction } from 'apollo-server-core'
import { Request } from 'express'
import { Container } from 'typedi'
import { AuthService } from '../services/auth.service'

interface ContextParams {
  req: Request
}

export interface Context {
  user?: { id: string; email: string }
}

export const getContext: ContextFunction = async ({
  req,
}: ContextParams): Promise<Context> => {
  const context: Context = {}

  const token = req.headers.authorization || ''
  const authService = Container.get(AuthService)

  try {
    context.user = await authService.verifyToken(token)
    // tslint:disable-next-line: no-empty
  } catch {}

  return context
}
