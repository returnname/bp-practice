import { ModelType } from 'typegoose'
import UserModel, { User } from '../models/user'
import { Service } from 'typedi'

@Service()
export class UserRepository {
  private readonly model: ModelType<User>

  constructor() {
    this.model = UserModel
  }

  async createOne(data: Partial<User>) {
    return this.model.create(data)
  }

  async find(selector: Partial<User> = {}) {
    return this.model.find(selector).populate('user')
  }

  async findOne(selector?: Partial<User>) {
    return this.model.findOne(selector)
  }

  async findOneById(_id: string) {
    return this.model.findById(_id)
  }

  async updateById(_id: string, updateFields: Partial<User>) {
    return this.model.findOneAndUpdate({ _id }, updateFields, { new: true })
  }
}
