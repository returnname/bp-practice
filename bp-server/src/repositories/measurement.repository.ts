import { ModelType } from 'typegoose'
import { Service } from 'typedi'
import MeasurementModel, { Measurement } from '../models/measurement'

@Service()
export class MeasurementRepository {
  private readonly model: ModelType<Measurement>

  constructor() {
    this.model = MeasurementModel
  }

  async find(selector?: Partial<Measurement>) {
    return this.model.find(selector).populate('user')
  }

  async createOne(data: Partial<Measurement>) {
    return this.model.create(data)
  }
}
