import 'reflect-metadata'
import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { connect } from 'mongoose'
import { buildSchema } from 'type-graphql'
import { Container } from 'typedi'
import { config as loadEnv } from 'dotenv'
import { ObjectId } from 'mongodb'

import { getContext as context } from './apollo/context.apollo'
import { ObjectIdScalar } from './apollo/object-id.scalar'
import { authChecker } from './apollo/authChecker'

const PORT = process.env.PORT || 4000

loadEnv()
;(async () => {
  connect(
    `${process.env.MONGO_URL}`,
    { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false }
  )

  const app = express()

  Container.set('jwt_secret', process.env.JWT_SECRET || 'SECRET')

  const schema = await buildSchema({
    resolvers: [__dirname + '/resolvers/*.ts'],
    container: Container,
    emitSchemaFile: true,
    scalarsMap: [{ type: ObjectId, scalar: ObjectIdScalar }],
    authChecker,
  })

  const server = new ApolloServer({
    schema,
    context,
  })

  server.applyMiddleware({ app })

  await app.listen(PORT)
  // tslint:disable-next-line:no-console
  console.log(`Server starts on ${PORT}!`)
})()
