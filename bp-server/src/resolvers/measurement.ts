import {
  Resolver,
  Query,
  Mutation,
  Arg,
  InputType,
  Field,
  Ctx,
  Authorized,
} from 'type-graphql'

import { MeasurementRepository } from '../repositories/measurement.repository'
import { UserRepository } from '../repositories/user.repository'
import { Context } from '../apollo/context.apollo'
import { Measurement } from '../models/measurement'
import { ObjectId } from 'mongodb'

@InputType()
class MeasurementInput implements Partial<Measurement> {
  @Field()
  systolic: number

  @Field()
  dystolic: number

  @Field()
  date: Date

  @Field({ nullable: true })
  comment: string

  @Field({ nullable: true })
  userId?: string
}

@Resolver(Measurement)
export class MeasurementResolver {
  constructor(
    protected measurementRepo: MeasurementRepository,
    protected userRepo: UserRepository
  ) {}

  @Authorized()
  @Query(() => [Measurement])
  async measurements(@Ctx() ctx: Context): Promise<Measurement[]> {
    const userId = ctx.user && ctx.user.id

    if (!userId) {
      throw new Error('User id not specified!')
    }

    const list = await this.measurementRepo.find({
      user: new ObjectId(userId),
    })

    return list
  }

  @Authorized()
  @Mutation(() => Measurement)
  async createMeasurement(
    @Arg('data') measurementData: MeasurementInput,
    @Ctx() ctx: Context
  ): Promise<Measurement> {
    const userId = measurementData.userId || (ctx.user && ctx.user.id)

    if (!userId) {
      throw new Error('User id not specified!')
    }

    const user = await this.userRepo.findOneById(userId)

    if (!user) {
      throw Error('User not founded!')
    }

    const measurement = await this.measurementRepo.createOne({
      ...measurementData,
      user,
    })

    return measurement
  }
}
