import {
  Resolver,
  Query,
  Arg,
  Mutation,
  InputType,
  Field,
  Args,
  ArgsType,
  ID,
  Ctx,
  Authorized,
} from 'type-graphql'
import { ObjectId } from 'mongodb'
import { Service } from 'typedi'
import { IsEmail, MinLength } from 'class-validator'

import { User, Role } from '../models/user'
import { UserRepository } from '../repositories/user.repository'
import { UserService } from '../services/user.service'
import { AuthService } from '../services/auth.service'
import { Context } from '../apollo/context.apollo'
import { ApolloError, AuthenticationError } from 'apollo-server-core'

@ArgsType()
class InputLogin {
  @Field()
  @IsEmail()
  email: string

  @Field()
  @MinLength(5)
  password: string
}

@InputType()
class InputRegister {
  @Field()
  @IsEmail()
  email: string

  @Field()
  @MinLength(5, { message: 'Password must be longer 5 characters' })
  password: string

  @Field()
  role: Role

  @Field()
  firstName: string

  @Field()
  lastName: string
}

@InputType()
class InputUpdate implements Partial<User> {
  @Field({ nullable: true })
  firstName?: string
  @Field({ nullable: true })
  lastName?: string
  @Field({ nullable: true })
  email?: string
}

@Service()
@Resolver(User)
export class UserResolver {
  constructor(
    private userRepository: UserRepository,
    private userService: UserService,
    private authService: AuthService
  ) {}

  @Query(() => [User])
  async users(): Promise<User[]> {
    const userList = await this.userRepository.find()

    return userList
  }

  @Authorized()
  @Query(() => User)
  async me(@Ctx() ctx: Context) {
    const userId = ctx.user && ctx.user.id

    if (!userId) {
      throw new ApolloError('User id not stated!')
    }

    const user = await this.userRepository.findOneById(userId)

    return user
  }

  @Mutation(() => User)
  async registerUser(@Arg('user') user: InputRegister) {
    const registeredUser = await this.userService.createNewUser(user)

    registeredUser.token = await this.authService.generateToken(registeredUser)

    return registeredUser
  }

  @Mutation(() => User)
  async loginUser(@Args() { email, password }: InputLogin) {
    const loginedUser = await this.userService.attemptLogin({
      email,
      password,
    })

    loginedUser.token = await this.authService.generateToken(loginedUser)

    return loginedUser
  }

  @Mutation(() => String)
  async addPatient(
    @Arg('doctor', () => ID) doctorId: ObjectId,
    @Arg('patient', () => ID) patientId: ObjectId
  ) {
    const str = await this.userService.addPatient(doctorId, patientId)

    return `${str}`
  }

  @Authorized()
  @Mutation(() => User)
  async updateProfile(@Arg('update') data: InputUpdate, @Ctx() ctx: Context) {
    const userId = ctx.user && ctx.user.id

    if (!userId) {
      throw new AuthenticationError('User id not founded')
    }

    const updatedUser = await this.userRepository.updateById(userId, data)

    return updatedUser
  }
}
