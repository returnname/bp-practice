import { ObjectType, Field, ID, registerEnumType } from 'type-graphql'
import { ObjectId } from 'mongodb'
import {
  Typegoose,
  prop,
  arrayProp,
  Ref,
  instanceMethod,
  InstanceType,
} from 'typegoose'

export enum Role {
  patient = 'patient',
  doctor = 'doctor',
}

registerEnumType(Role, {
  name: 'Role',
})

@ObjectType()
export class User extends Typegoose {
  @Field(() => ID)
  readonly _id: ObjectId

  @prop({ required: true })
  password: string

  @prop({ required: true, lowercase: true, unique: true })
  @Field()
  email: string

  @prop()
  @Field({ nullable: true })
  createdAt: Date

  @prop()
  @Field({ nullable: true })
  lastVisit: Date

  @Field({ nullable: true })
  token: string

  @prop({ required: true, lowercase: true })
  @Field(() => Role)
  role: Role

  @prop({ required: true })
  @Field()
  firstName: string

  @prop({ required: true })
  @Field()
  lastName: string

  @arrayProp({ itemsRef: User, unique: true })
  @Field(() => [User], { nullable: true })
  patients?: Array<Ref<User>>

  @prop({ ref: User })
  @Field(() => User, { nullable: true })
  doctor?: Ref<User>

  @instanceMethod
  addNewPatient(this: InstanceType<User>, patient: InstanceType<User>) {
    if (this.role !== Role.doctor) {
      throw new Error('Only doctor should have patients!')
    }

    if (!this.patients) {
      this.patients = []
    }

    // typegoose not support unique for array props
    this.patients = [...new Set([...this.patients, patient])]

    // set to patient doctor relation
    patient.doctor = this._id
    patient.save()

    return this.save()
  }
}

export default new User().getModelForClass(User, {
  schemaOptions: { timestamps: true },
})
