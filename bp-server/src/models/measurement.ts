import { prop, Typegoose, Ref } from 'typegoose'
import { ObjectType, Field, ID } from 'type-graphql'
import { ObjectId } from 'mongodb'
import { User } from './user'

@ObjectType()
export class Measurement extends Typegoose {
  @Field(() => ID)
  readonly _id: ObjectId

  @prop({ required: true })
  @Field()
  systolic: number

  @prop({ required: true })
  @Field()
  dystolic: number

  @prop({ required: true })
  @Field(() => Date)
  date: Date

  @prop()
  @Field({ nullable: true })
  comment?: string

  @prop({ ref: User })
  @Field(() => User)
  user: Ref<User> | ObjectId
}

export default new Measurement().getModelForClass(Measurement, {
  schemaOptions: { timestamps: true },
})
