import { connect } from 'mongoose'
import MeasurementModel from './models/measurement'
import UserModel from './models/user'
import { UserService } from './services/user.service'
import { Container } from 'typedi'

// tslint:disable: no-var-requires
// const measureMocks = require('./__mocks__/measurements.json')
const userMocks = require('./__mocks__/users.json')

async function migrate() {
  await connect(
    'mongodb://192.168.99.100:27017/bp',
    { useNewUrlParser: true }
  )
  Container.set('jwt_secret', process.env.JWT_SECRET || 'SECRET')

  const userService = Container.get(UserService)
  await UserModel.deleteMany({})
  await Promise.all(userMocks.map((u: any) => userService.createNewUser(u)))

  await MeasurementModel.deleteMany({})
  // await MeasurementModel.create(
  //   measureMocks.map((m: any) => ({ ...m, user: users[0] }))
  // )

  // tslint:disable-next-line: no-console
  console.log('Migrate successful!')

  process.exit(0)
}

migrate()
