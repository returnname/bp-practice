import { Service, Inject } from 'typedi'
import { promisify } from 'util'
import { compare } from 'bcryptjs'
import { User } from '../models/user'

// tslint:disable-next-line: no-var-requires
const jwt = require('jsonwebtoken')
const signAsync = promisify(jwt.sign)
const verifyAsync = promisify(jwt.verify)

@Service()
export class AuthService {
  @Inject('jwt_secret')
  private readonly jwtSecret: string

  async verifyToken(token: string): Promise<{ id: string; email: string }> {
    let user
    try {
      const { id, email } = await verifyAsync(token, this.jwtSecret)
      user = { id, email }
    } catch (error) {
      throw new Error(`Error verifying token : ${error}`)
    }

    return user
  }

  async generateToken({ _id, email }: User) {
    let token

    try {
      token = await signAsync({ id: _id, email }, this.jwtSecret, {
        expiresIn: '24h',
      })
    } catch (error) {
      throw new Error(`Error encoding token : ${error}`)
    }

    return token
  }

  async validateCredentionals(
    password: string,
    comparePassword: string
  ): Promise<boolean> {
    const isSuccess = await compare(password, comparePassword)

    return isSuccess
  }
}
