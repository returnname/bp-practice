import { User, Role } from '../models/user'
import { UserRepository } from '../repositories/user.repository'
import { Service } from 'typedi'
import { hash, genSalt } from 'bcryptjs'
import { ObjectId } from 'mongodb'
import { ApolloError } from 'apollo-server-express'
import { AuthService } from './auth.service'

type RegisterData = Credentials & Partial<User>

export interface Credentials {
  email: string
  password: string
}

@Service()
export class UserService {
  constructor(
    protected repo: UserRepository,
    protected authService: AuthService
  ) {}

  /**
   * TODO: Move to auth service
   */
  async attemptLogin({ email, password }: Credentials) {
    const user = await this.repo.findOne({ email })

    if (!user) {
      throw new ApolloError('User not found!', 'LOGIN_ERROR', {
        message: 'User not founded with this email',
      })
    }

    const passwordMatched = await this.authService.validateCredentionals(
      password,
      user.password
    )

    if (!passwordMatched) {
      throw new Error('Password is incorrect!')
    }

    return user
  }

  async createNewUser({ password, ...userInfo }: RegisterData): Promise<User> {
    const salt = await genSalt(10)
    const passwordHash = await hash(password, salt)

    const user = await this.repo.createOne({
      ...userInfo,
      password: passwordHash,
      createdAt: new Date(),
      lastVisit: new Date(),
    })

    return user
  }

  async addPatient(doctorId: ObjectId, patientId: ObjectId) {
    const doctor = await this.repo.findOne({ _id: doctorId, role: Role.doctor })
    if (!doctor) {
      throw new Error('Doctor not founded!')
    }

    const patient = await this.repo.findOne({
      _id: patientId,
      role: Role.patient,
    })
    if (!patient) {
      throw new Error('Patient not founded!')
    }

    await doctor.addNewPatient(patient)

    return 'Added new patient'
  }
}
