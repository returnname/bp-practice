declare module 'reflexbox/styled-components' {
  import * as React from 'react'
  import * as SS from 'styled-system'

  type BoxProps = SS.LayoutProps &
    SS.SpaceProps &
    SS.TypographyProps &
    SS.ColorProps &
    SS.FlexboxProps &
    React.DOMAttributes

  export class Flex extends React.Component<BoxProps> {}
  export class Box extends React.Component<BoxProps> {}
}
