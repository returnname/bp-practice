import 'styled-components'
declare module 'styled-components' {

  export type Mixin = Interpolation<ThemeProps>
  export interface ThemeInterface {
    mixins: {
      [key: string]: Mixin
    }
  }

  export type StyledMixinsProps = {
    border?: string
  }

  export type StyledProps = ThemedStyledProps<StyledMixinsProps, ThemeInterface>

  export type MixinFunction = (props: ThemeProps) => Mixin;

  export type ReactStyledProps<T> = T & React.HTMLProps<HTMLDivElement> & { ref?: any; as?: any }
}
