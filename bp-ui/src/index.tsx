import 'react-hot-loader'
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'

import App from './application'

type RootNode = HTMLElement | null

const el: RootNode = document.getElementById('root')

const render = (root: RootNode) => {
  if (!root) {
    throw new Error('No root node founded!')
  }

  ReactDOM.render(
    <Router>
      <App />
    </Router>,
    root
  )
}

render(el)
