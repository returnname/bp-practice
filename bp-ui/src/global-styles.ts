import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';

import { themeVars } from '@ui/theme/light';
import 'react-day-picker/lib/style.css';

export const GlobalStyles = createGlobalStyle`
  ${normalize}
  

  @import url('https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap');
  
  body {
    font-family: 'Roboto', sans-serif;

    
    -webkit-font-smoothing: antialiased;
  }

  *{
    box-sizing: border-box;
  }

  *:focus{
    outline: none;
  }

  :root {
   ${themeVars}
  }

  #root{
    /* display: flex;
    flex-flow: column nowrap;
    align-items: stretch; */
    height: 100vh;
    overflow: hidden;
    color: var(--primary-text);
  }
`