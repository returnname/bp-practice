import React from 'react'
import { hot } from 'react-hot-loader/root'
import { Provider as StoreProvider } from 'react-redux'
import { ConnectedRouter as RouterProvider } from 'connected-react-router'
import { ThemeProvider } from 'styled-components'
import { ApolloProvider } from '@apollo/react-hooks'

import { GlobalStyles } from './global-styles'
import { Routes } from './routes'
import { theme } from '@ui/theme/light'
import { history, store } from './store'
import { client } from './apollo'
import { createHub } from './components/message-hub'
import { Provider as MessageProvider } from './components/message-hub/provider'

const messageHub = createHub()

export const App = () => {
  return (
    <ApolloProvider client={client}>
      <StoreProvider store={store}>
        <RouterProvider history={history}>
          <ThemeProvider theme={theme}>
            <MessageProvider hub={messageHub}>
              <>
                <GlobalStyles />
                <Routes />
              </>
            </MessageProvider>
          </ThemeProvider>
        </RouterProvider>
      </StoreProvider>
    </ApolloProvider>
  )
}

export default hot(App)
