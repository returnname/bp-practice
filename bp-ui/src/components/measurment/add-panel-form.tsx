import React from 'react'
import DatePicker from 'react-day-picker'

import { Formik, Form, Field, FieldProps, FormikProps } from 'formik'

import * as Yup from 'yup'

import { Input, Textarea, ButtonGroup, Button } from '@ui'
import { Flex } from 'reflexbox/styled-components'

interface FormProps {
  comment: string
  date: Date
  dystolic: number | ''
  systolic: number | ''
  timeOfDay: 'e' | 'm' | ''
}

const measurmentScheme = Yup.object().shape({
  comment: Yup.string(),
  date: Yup.date().required(),
  dystolic: Yup.number().required(),
  systolic: Yup.number().required(),
  timeOfDay: Yup.string()
    .oneOf(['m', 'e'])
    .required(),
})

export const AddPanelForm = ({ handleSubmit }: any) => (
  <Formik
    initialValues={
      {
        comment: '',
        date: new Date(),
        timeOfDay: '',
        systolic: '',
        dystolic: '',
      } as FormProps
    }
    validationSchema={measurmentScheme}
    onSubmit={(values, form) => {
      handleSubmit(values)
      form.resetForm()
    }}
    render={({
      handleChange,
      setFieldValue,
      values,
      errors,
      isValid,
      touched,
      setFieldTouched,
    }: FormikProps<FormProps>) => (
      <Flex flexDirection="column" p="15px">
        <Form>
          <Input
            name="systolic"
            type="number"
            placeholder="Systolic"
            label="Sys"
            value={values.systolic}
            onChange={e => (
              !touched.systolic && setFieldTouched('systolic', true),
              handleChange(e)
            )}
            hasError={touched.systolic && !!errors.systolic}
          />
          <Input
            name="dystolic"
            type="number"
            placeholder="Dystolic"
            label="Dys"
            value={values.dystolic}
            onChange={e => (
              !touched.dystolic && setFieldTouched('dystolic', true),
              handleChange(e)
            )}
            hasError={touched.dystolic && !!errors.dystolic}
          />

          <Input
            label="Day"
            render={() => (
              <Field
                name="date"
                render={({ field }: FieldProps) => (
                  <DatePicker
                    selectedDays={field.value}
                    onDayClick={d => setFieldValue('date', d)}
                  />
                )}
              />
            )}
          />

          <Input
            label="Time of day"
            hasError={touched.timeOfDay && !!errors.timeOfDay}
            render={() => (
              <ButtonGroup>
                <Button
                  type="button"
                  active={values.timeOfDay === 'm'}
                  onClick={() => (
                    !touched.timeOfDay && setFieldTouched('timeOfDay', true),
                    setFieldValue(
                      'timeOfDay',
                      values.timeOfDay !== 'm' ? 'm' : ''
                    )
                  )}
                >
                  Morning
                </Button>
                <Button
                  type="button"
                  active={values.timeOfDay === 'e'}
                  onClick={() => (
                    !touched.timeOfDay && setFieldTouched('timeOfDay', true),
                    setFieldValue(
                      'timeOfDay',
                      values.timeOfDay !== 'e' ? 'e' : ''
                    )
                  )}
                >
                  Evening
                </Button>
              </ButtonGroup>
            )}
          />

          <Textarea
            name="comment"
            placeholder="Your feelings"
            label="Comment"
            onChange={handleChange}
            value={values.comment}
          />

          <Button type="submit" disabled={!isValid} width="100%">
            Add record
          </Button>
        </Form>
      </Flex>
    )}
  />
)
