import React, { FunctionComponent } from 'react'
import styled from 'styled-components'

const PreassureContainer = styled.div`
  width: 100px;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  margin-left: 10px;
  border-radius: var(--border-radius-box);
  background: linear-gradient(135deg, rgb(245, 147, 151) 0%, #f5576c 100%);
  color: var(--secondary-text);

  ${props => props.theme.mixins.border}

  &:first-child {
    margin-left: 0;
  }
`
const Value = styled.span`
  font-size: 37px;
  font-weight: 700;
`
const Name = styled.span`
  position: absolute;
  top: 2px;
  left: 5px;
  font-size: 14px;
`

interface PreassureProps {
  name: 'systolic' | 'dystolic'
  value: number | ''
}

export const Preassure: FunctionComponent<PreassureProps> = ({
  value,
  name,
}) => (
  <PreassureContainer>
    <Name>{name}</Name>
    <Value>{value}</Value>
  </PreassureContainer>
)
