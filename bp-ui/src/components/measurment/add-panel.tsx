import React from 'react'
import styled from 'styled-components'

import { ChevronsRight } from 'react-feather'
import { useSpring, animated } from 'react-spring'

import { AddPanelForm as Form } from './add-panel-form'
import { MeasurementModel } from '../../store/types'

const Container = styled(animated.div)`
  text-align: left;
  border-radius: 0;
  position: absolute;
  width: 300px;
  top: 0;
  right: 0;
  height: 100%;
  background-color: var(--primary);
  padding: 0;
  z-index: 10;

  ${props => props.theme.mixins.border}
  border-top: none;
  border-bottom: none;
`

const ToggleButton = styled(animated(ChevronsRight))`
  position: absolute;
  top: 5px;
  right: -50px;

  &:hover {
    cursor: pointer;
  }
`

interface PanelProps {
  visible?: boolean
  toggle: () => void
  handleSubmit: (m: MeasurementModel) => void
}

export const Panel = ({ visible, toggle, handleSubmit }: PanelProps) => {
  const animate = useSpring({
    x: visible ? 320 : 0,
    right: visible ? -70 : -50,
    spin: visible ? 180 : 0,
  })

  return (
    <Container
      style={{
        transform: animate.x.interpolate(x => `translate3D(${x}px, 0, 0)`),
      }}
    >
      <Form handleSubmit={handleSubmit} />
      <ToggleButton
        onClick={() => toggle()}
        size="50px"
        style={{
          right: animate.right.interpolate(r => `${r}px`),
          transform: animate.spin.interpolate(deg => `rotateY(-${deg}deg)`),
        }}
      />
    </Container>
  )
}
