import React from 'react'
import styled from 'styled-components'

import { Card } from '@ui/card'
import { Preassure } from './preassure'
import { Label } from '@ui/label'
import { MeasurementModel } from 'store/types'

const Container = styled(Card)`
  &:hover {
    cursor: pointer;
    box-shadow: 2px 5px 15px -10px rgba(0, 0, 0, 0.3);
  }
`
const InfoDate = styled.div`
  font-size: 18px;
`
const InfoComment = styled.p`
  font-size: 20px;
  padding: 10px 0;
  margin: 0;
`

const InfoLabel = styled(Label)`
  position: absolute;
  right: 0;
  top: 0;
`

const Info = styled.div`
  flex-grow: 1;
  padding: 0 20px;
  position: relative;

  & > ${InfoDate} {
    text-align: left;
  }
`

type MeasurementCardProps = MeasurementModel

const labelString = (timeOfDay: MeasurementCardProps['timeOfDay']) =>
  timeOfDay === 'm' ? 'Morning' : timeOfDay === 'e' ? 'Evening' : ''

export const MeasurmentCard = ({
  dystolic,
  systolic,
  date,
  comment,
  timeOfDay,
}: MeasurementCardProps) => (
  <Container justify="flex-start" align={'flex-start'}>
    <Preassure name="systolic" value={systolic} />
    <Preassure name="dystolic" value={dystolic} />

    <Info>
      <InfoDate>{new Date(date).toLocaleDateString()}</InfoDate>
      <InfoComment>{comment}</InfoComment>
      <InfoLabel>{labelString(timeOfDay)}</InfoLabel>
    </Info>
  </Container>
)
