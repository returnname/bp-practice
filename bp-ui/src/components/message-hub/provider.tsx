import React, { ReactNode, useMemo, useState } from 'react'
import { HubContext, Hub } from './'

interface ProviderProps {
  children: ReactNode
  hub: Hub
}

export function Provider({ children, hub }: ProviderProps) {
  const Context = HubContext

  const [_, set] = useState()
  const up = () => set({})

  const contextValue = useMemo<Hub>(() => {
    hub.observe(() => {
      up()
    })

    return { ...hub }
  }, [_])

  return <Context.Provider value={contextValue}>{children}</Context.Provider>
}
