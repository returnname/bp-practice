import React, { useContext, useMemo } from 'react'

export enum MessageType {
  error = 'error',
  success = 'success',
}

// tslint:disable-next-line: interface-over-type-literal
type Message = { text: string; type: MessageType }

export interface SubHubs {
  [key: string]: Message[]
}

export interface Hub {
  subhubs: SubHubs
  addHub: (name: string) => void
  add: (name: string, message: Message) => void
  remove: (name: string, index: number) => void
  observe: (fn: () => void) => void
}

export const HubContext = React.createContext<Hub>({
  subhubs: {},
  addHub: () => ({}),
  add: () => ({}),
  remove: () => ({}),
  observe: () => {},
})

export function createHub() {
  const subhubs: SubHubs = {}
  let listener: () => void = () => {}

  function addHub(name: string) {
    subhubs[name] = []
  }

  function add(name: string, message: Message) {
    if (!subhubs[name]) {
      addHub(name)
    }

    subhubs[name].push(message)

    listener()
  }

  function remove(name: string, index: number) {
    subhubs[name].splice(index, 1)

    listener()
  }

  function observe(fn: () => void) {
    listener = fn
  }

  return { add, addHub, subhubs, remove, observe }
}

export const useHub = (name: string = 'name') => {
  const { subhubs, add, remove, addHub } = useContext(HubContext)

  if (!subhubs[name]) {
    addHub(name)
  }

  const messages = useMemo(() => subhubs[name], [subhubs[name]])

  const notice = (text: string, type: MessageType = MessageType.success) => {
    add(name, { text, type })
  }

  const error = (text: string) => {
    notice(text, MessageType.error)
  }

  const close = (id: number) => {
    remove(name, id)
  }

  return { messages, notice, close, error }
}
