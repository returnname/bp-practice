import React, { FunctionComponent } from 'react'
import { RouteComponentProps, Redirect } from 'react-router'

import { useQuery } from '@apollo/react-hooks'
import { IS_LOGGED_IN } from '../../graphql/auth/queries'

type AuthorizedHOC<P> = (
  Component: FunctionComponent<P>
) => FunctionComponent<P>

export const redirectIfAuth: AuthorizedHOC<
  RouteComponentProps
> = WrappedComponent => {
  return props => {
    const { data } = useQuery(IS_LOGGED_IN)

    if (data.isLoggedIn) {
      return <Redirect to="/" />
    }

    return <WrappedComponent {...props} />
  }
}
