import React from 'react'
import styled from 'styled-components'

const Container = styled.header`
  width: 100%;
  height: 50px;
  background-color: #000;
`

export const Header = () => <Container>header</Container>
