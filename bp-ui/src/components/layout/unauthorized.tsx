import React, { ReactNode } from 'react'
import styled from 'styled-components'
import { Flex, Box } from 'reflexbox/styled-components'

const Container = styled(Flex)`
  background-color: var(--background);
  overflow: hidden;
  height: 100vh;
`

const Content = styled(Box).attrs({ as: 'main' })`
  width: 500px;
  background-color: var(--primary);
  height: max-content;
`

const Top = styled.div``

interface LayoutProps {
  children?: ReactNode
  top?: ReactNode
}

export const UnauthorizedLayout = ({ children, top }: LayoutProps) => (
  <Container justifyContent="center" alignItems="center" flexDirection="column">
    <Top>{top && <>{top}</>}</Top>
    <Content>
      <>{children}</>
    </Content>
  </Container>
)
