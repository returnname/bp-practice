import React, { ReactElement, ReactNode } from 'react'
import styled from 'styled-components'
import { Home, User } from 'react-feather'
import { Link } from 'react-router-dom'

const Container = styled.ul`
  width: 100%;
  margin: 0;
  padding: 0;
`

const ItemIcon = styled.span``

const StyledLink = styled(Link).attrs({})`
  text-decoration: none;
  color: var(--primary-text);
`
export const ItemContainer = styled.li`
  padding: 15px 10px;
  list-style: none;
  border-top: 1px solid var(--border);
  font-size: 18px;
  transition: all 0.2s;
  text-align: left;
  position: relative;

  & > ${ItemIcon} {
    position: absolute;
    top: 15px;
    right: 10px;
  }

  &:hover {
    transition: all 0.2s;
    background: var(--background);
    cursor: pointer;
  }

  &:first-child {
    border-top: none;
  }

  &:last-child {
    border-bottom: 1px solid var(--border);
  }
`

interface ItemProps {
  children?: ReactNode
  icon?: ReactElement
}

const MenuItem = ({ children, icon }: ItemProps) => (
  <ItemContainer>
    {children}
    {icon && <ItemIcon> {icon} </ItemIcon>}
  </ItemContainer>
)

export const Menu = () => (
  <Container>
    <MenuItem icon={<Home size={18} />}>
      <StyledLink to="/"> Home</StyledLink>
    </MenuItem>
    <MenuItem icon={<User size={18} />}>
      <StyledLink to="/profile"> Profile</StyledLink>
    </MenuItem>
  </Container>
)
