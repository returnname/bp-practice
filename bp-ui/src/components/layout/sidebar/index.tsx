import React from 'react'
import styled from 'styled-components'

import { Menu, ItemContainer } from './menu'
import { Logo } from './logo'
import { UserX } from 'react-feather'

const Wrapper = styled.aside`
  width: 100%;
  border-right: 1px solid rgb(235, 236, 237);
  background-color: var(--primary);
  padding: 12px 0px;
  height: 100vh;
  max-width: 275px;
  position: relative;
`

const Logout = styled(ItemContainer).attrs({ as: 'div' })`
  position: absolute;
  bottom: 0;
  padding: 10px;
  text-align: center;
  width: 100%;
`

interface SidebarProps {
  handleLogout: () => void
}

export const Sidebar = ({ handleLogout }: SidebarProps) => (
  <Wrapper>
    <Logo />
    <Menu />
    <Logout onClick={handleLogout}>
      Logout <UserX size={15} />
    </Logout>
  </Wrapper>
)
