import React from 'react'
import styled from 'styled-components'

import { Box } from 'reflexbox/styled-components'

const LogoContainer = styled(Box)`
  font-size: 25px;
  color: var(--primary-text);
  font-weight: bold;
  text-align: center;
  padding: 20px;

  & > span {
    color: #ffb6c1;
  }
`

export const Logo = () => (
  <LogoContainer>
    <>Hyper </>
    <span>Nation</span>
  </LogoContainer>
)
