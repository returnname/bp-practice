import React, { ReactElement, ReactNode } from 'react'
import styled from 'styled-components'

import { useSpring, animated } from 'react-spring'

import { Sidebar } from './sidebar/index'
import { logout } from '../../apollo'

const Grid = styled.div`
  display: flex;
  background-color: var(--background);
  overflow: auto;
  height: 100%;
`

const Body = styled.main`
  width: 100%;
  max-height: 100%;
  overflow: hidden;
`

const Content = styled.div`
  width: 100%;
  height: inherit;
  padding: 10px;
  background-color: var(--primary);
  border-radius: 0px;
  position: relative;
  height: 100%;
  display: flex;
  flex-direction: column;
  z-index: 20;

  ${props => props.theme.mixins.border}
  border-top: none;
  border-bottom: none;
`

interface MainLayoutProps {
  children?: ReactNode
  right?: ReactNode
}

const ContentBase = styled(animated.div)`
  margin-left: 100px;
  max-width: 700px;
  height: 100%;
  position: relative;
`

const MainLayout = ({ children, right }: MainLayoutProps): ReactElement => {
  const animateProps = useSpring({
    from: { position: 'absolute', left: '-100%' },
    to: { position: 'relative', left: '0' },
  })

  return (
    <Grid>
      <Sidebar handleLogout={() => logout()} />
      <Body>
        <ContentBase style={animateProps}>
          <Content>{children}</Content>
          {right && <>{right}</>}
        </ContentBase>
      </Body>
    </Grid>
  )
}

export { MainLayout }
