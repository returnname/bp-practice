import React, {
  ReactElement,
  ReactNode,
  useLayoutEffect,
  useState,
} from 'react'
import { SortList, SortTypes } from '../../store/types'
import styled from 'styled-components'

import { useSprings, animated } from 'react-spring'
import { Box } from 'reflexbox/styled-components'

const Container = styled.div`
  overflow-y: auto;
  height: 100%;
  &::-webkit-scrollbar-track {
    box-shadow: none;
    background-color: var(--background);
  }

  &::-webkit-scrollbar {
    width: 5px;
    background-color: var(--background);
  }

  &::-webkit-scrollbar-thumb {
    background-color: #e0e0e0;
  }
`

const ListItem = styled(animated.div)`
  margin-bottom: 10px;

  &:last-child {
    margin-bottom: 0px;
  }
`

function compare<M>(a: M, b: M, type: SortTypes): boolean {
  if (type === 'ASC') {
    return a > b
  }

  if (type === 'DESC') {
    return a < b
  }

  return false
}

function sortByField<I>(field: keyof I, type: SortTypes) {
  return (cur: I, next: I) => (compare(cur[field], next[field], type) ? -1 : 0)
}

interface ListProps<I> {
  list: I[]
  sort?: SortList<I>
  render: (m: I, key: string | number) => ReactNode
}

export function ItemsList<T extends object>({
  list = [],
  sort,
  render,
}: ListProps<T>): ReactElement {
  if (sort) {
    list = list.sort(sortByField(sort.field, sort.type))
  }

  const [refMap] = useState(new WeakMap())
  const [rectMap] = useState(new WeakMap())

  const [springs, set] = useSprings(list.length, () => ({
    y: 0,
    reset: true,
    config: { friction: 45 },
  }))

  useLayoutEffect(() => {
    const diffMap = list.map(i => {
      const last = rectMap.get(i) || null
      const current = refMap.get(i).getBoundingClientRect()

      rectMap.set(i, current)

      return last ? last.top - current.top : 0
    })

    // @ts-ignore
    set((index: number) => ({ from: { y: diffMap[index] }, to: { y: 0 } }))
  })

  return (
    <Container>
      {list.length === 0 && <Box p="20px">No items founded! </Box>}

      {springs.map(({ y }: any, index) => (
        <ListItem
          key={index}
          ref={ref => refMap.set(list[index], ref)}
          style={{
            transform: y.interpolate((val: number) => `translateY(${val}px) `),
            zIndex: list.length - index,
          }}
        >
          {render(list[index], index)}
        </ListItem>
      ))}
    </Container>
  )
}
