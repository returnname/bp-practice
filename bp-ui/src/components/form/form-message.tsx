import React from 'react'
import styled from 'styled-components'

import { Flex } from 'reflexbox/styled-components'
import { Check, AlertCircle } from 'react-feather'

import { MessageType } from '../message-hub'

const Container = styled(Flex)`
  width: 100%;
  color: var(--primary-text);
  background-color: ${props =>
    props.type === MessageType.success ? '#c6ffb8' : '#ffc6b8'};
  font-size: 15px;

  & > p {
    margin: 0;
  }

  &:hover {
    cursor: pointer;
    transition: ease-in 0.1s;
    background-color: ${props =>
      props.type === MessageType.success ? '#ccffcf' : '#ffccc6'};
  }
`

export const FormMessage = ({
  text = 'text',
  handleClick,
  type,
}: {
  text: string
  handleClick: () => void
  type: MessageType
}) => (
  <Container
    p="10px"
    justifyContent="space-between"
    alignItems="center"
    onClick={handleClick}
    type={type}
  >
    <p>{text}</p>

    {type === MessageType.success && <Check size="15px" />}
    {type === MessageType.error && <AlertCircle size="15px" />}
  </Container>
)
