import React from 'react'
import { useHub } from '../message-hub'
import { FormMessage } from './form-message'

export const Hub = ({ hubId: formName }: { hubId: string }) => {
  const { messages, close } = useHub(formName)

  return (
    <>
      {messages.map((t, index) => (
        <FormMessage
          key={formName + index}
          text={t.text}
          type={t.type}
          handleClick={() => close(index)}
        />
      ))}
    </>
  )
}
