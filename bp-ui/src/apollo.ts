import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link'
import {
  changeFilter,
  initialState as measurementState,
} from './graphql/measurements/mutations'

const cache = new InMemoryCache()

const httpLink = createHttpLink({
  uri: 'http://localhost:4000/graphql',
})

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('token')

  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : '',
    },
  }
})

export const client = new ApolloClient({
  link: ApolloLink.from([authLink, httpLink]),
  cache,
  connectToDevTools: true,
  resolvers: {
    Mutation: {
      changeFilter,
    },

    Measurement: {
      date: ({ date }) => new Date(date),
      timeOfDay: ({ date }) => (new Date(date).getUTCHours() > 12 ? 'e' : 'm'),
    },
  },
})

cache.writeData({
  data: {
    isLoggedIn: !!localStorage.getItem('token'),

    ...measurementState,
  },
})

export const login = (token: string) => {
  client.writeData({ data: { isLoggedIn: true } })
  localStorage.setItem('token', token)
}

export const logout = () => {
  client.resetStore()
  localStorage.removeItem('token')
}
