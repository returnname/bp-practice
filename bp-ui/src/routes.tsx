import React, { Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'

import { NotFound } from './pages/not-found'

const MainPage = React.lazy(() => import('./pages/main/index'))
const ProfilePage = React.lazy(() => import('./pages/profile/index'))
const LoginPage = React.lazy(() => import('./pages/auth/login'))
const RegisterPage = React.lazy(() => import('./pages/auth/register'))

export const Routes = () => {
  return (
    <Switch>
      <Suspense fallback={'loading'}>
        <Route path="/profile" component={ProfilePage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/register" component={RegisterPage} />
        <Route exact path="/" component={MainPage} />
        <Route render={() => <NotFound />} />
      </Suspense>
    </Switch>
  )
}
