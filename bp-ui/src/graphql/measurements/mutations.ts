import { MeasurementModel, SortList } from '../../store/types'
import { InMemoryCache } from 'apollo-cache-inmemory'

type WithType<P> = P & { __typename: string }

export interface MeasurementState {
  measurementSort: WithType<SortList<MeasurementModel>>
  measurementFilter: WithType<{
    dateStart?: Date
    dateEnd?: Date
    timeOfDay?: 'm' | 'e'
  }>
}

export const initialState: MeasurementState = {
  measurementSort: {
    __typename: 'SortList',
    field: 'date',
    type: 'ASC',
  },
  measurementFilter: {
    __typename: 'MeasurementFilter',
    dateStart: new Date(),
  },
}

export const changeFilter = (
  _: any,
  { field, type }: { field: string; type: string },
  { cache }: { cache: InMemoryCache }
) => {
  const data = {
    measurementSort: {
      __typename: 'SortList',
      field,
      type,
    },
  }

  cache.writeData({ data })
}
