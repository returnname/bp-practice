import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { routerMiddleware, connectRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'


import { reducer as measureReducer } from './measurement/reducer'
import { reducer as authReducer } from './auth/reducer'

const initialState = {};

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory()

const reducer = combineReducers({
  measurements: measureReducer,
  router: connectRouter(history),
  auth: authReducer
});


export const configureStore = (preloadedState = {}) => {
  const middlewares = [routerMiddleware(history)];

  return createStore(
    reducer,
    preloadedState,
    composeEnhancers(applyMiddleware(...middlewares))
  );
}

export const store = configureStore(initialState);
