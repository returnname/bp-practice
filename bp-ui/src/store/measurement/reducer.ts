import {
  AddMeasurmentAction,
  ADD_MEASUREMENT,
  EditSortAction,
  EDIT_SORT,
} from './actions'
import { MeasurementModel } from '../types'

const mockData = require('./mock.json')
const initList = mockData.map((m: any) => ({ ...m, date: new Date(m.date) }))

export interface MeasurementState {
  list: MeasurementModel[]
  // sort: SortList<MeasurementModel>,
  filter: {
    dateStart?: Date
    dateEnd?: Date
    timeOfDay?: 'm' | 'e'
  }
}

const initialState: MeasurementState = {
  list: initList,
  // sort: ['date', 'ASC'],
  filter: {},
}

type Action = AddMeasurmentAction | EditSortAction

export const reducer = (
  state: MeasurementState = initialState,
  action: Action
) => {
  switch (action.type) {
    case ADD_MEASUREMENT:
      return { ...state, list: [...state.list, action.measurement] }
    case EDIT_SORT:
      return { ...state, sort: action.sort }
    default:
      return state
  }
}
