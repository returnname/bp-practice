import { MeasurementModel, SortList } from '../types'

export const ADD_MEASUREMENT = 'ADD_MEASUREMENT';
export interface AddMeasurmentAction {
  type: typeof ADD_MEASUREMENT,
  measurement: MeasurementModel
}


export const EDIT_SORT = 'EDIT_SORT';
export interface EditSortAction {
  type: typeof EDIT_SORT,
  sort: SortList<MeasurementModel>
}

