
export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT';
export interface LoginAttemptAction {
  type: typeof LOGIN_ATTEMPT
}


export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export interface LoginSuccessAction {
  type: typeof LOGIN_SUCCESS
}

export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export interface LoginFailureAction {
  type: typeof LOGIN_FAILURE
}
