import {
  LoginAttemptAction,
  LoginSuccessAction,
  LoginFailureAction,
  LOGIN_ATTEMPT,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from './actions'

export interface AuthState {
  isAuth: boolean
  attempt: boolean
  error: boolean
}

const initialState: AuthState = {
  isAuth: false,
  attempt: false,
  error: false,
}

type Action = LoginAttemptAction & LoginSuccessAction & LoginFailureAction

export const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case LOGIN_ATTEMPT:
      return { ...state, isAuth: false, attempt: true }
    case LOGIN_SUCCESS:
      return { ...state, isAuth: true, attempt: false }
    case LOGIN_FAILURE:
      return { ...state, isAuth: false, attempt: false, error: true }
  }

  return state
}
