import { useDispatch } from 'react-redux'

import {
  LOGIN_ATTEMPT,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from './actions'

const apiLogin = (props: any): Promise<{ message: string, login: string }> => {
  return new Promise(resolve => {
    setTimeout(() => resolve({
      message: 'sign on!',
      login: props.login
    }), 500)
  })
}


interface Credentials {
  login: string
  password: string
}


export const useLoginAction = () => {
  const dispatch = useDispatch()

  const attemptLogin = async ({ password, login }: Credentials) => {
    dispatch({ type: LOGIN_ATTEMPT })
    try {
      const data = await apiLogin({ password, login });
      dispatch({ type: LOGIN_SUCCESS })

      return { data, error: null };
    } catch (error) {
      dispatch({ type: LOGIN_FAILURE })

      return { error };
    }
  };

  return attemptLogin;
}