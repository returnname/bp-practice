import { RouterState } from 'connected-react-router'
import { MeasurementState } from './measurement/reducer'
import { AuthState } from './auth/reducer'

export type SortTypes = 'ASC' | 'DESC'
export interface SortList<I> {
  field: keyof I
  type: SortTypes
}

export interface MeasurementModel {
  comment: string
  date: Date
  dystolic: number | ''
  systolic: number | ''
  timeOfDay: 'e' | 'm' | ''
  key: number
}

export interface State {
  router: RouterState
  measurements: MeasurementState
  auth: AuthState
}
