import styled from 'styled-components'

// TODO: Rewrite
type StyledTernary<K, S> = (
  v: K[keyof K] | undefined,
  d: S
) => K extends undefined ? S : K[keyof K]

const is: StyledTernary<CardProps, string> = (v, d) => (v !== undefined ? v : d)

interface CardProps {
  justify?: string
  align?: string
}

export const Card = styled.div<CardProps>`
  background-color: var(--primary);
  padding: 10px;
  width: 100%;
  flex: 1;
  display: flex;
  flex-direction: initial;
  justify-content: ${props => is(props.justify, 'center')};
  justify-content: ${props => is(props.align, 'center')};
  align-content: center;
  border-radius: var(--border-radius-box);

  ${props => props.theme.mixins.border}
  ${props => props.theme.mixins.transition}
`
