import React, { useRef, FunctionComponent } from 'react'
import styled, { ReactStyledProps, css } from 'styled-components'
import { FieldProps, FormikErrors } from 'formik'

interface InputFieldProps {
  borderRadius?: 'string'
}

export const Field = styled.input<InputFieldProps & InputProps>`
  display: block;
  padding: 10px;
  border-radius: ${props => (props.borderRadius ? props.borderRadius : '0px')};
  margin: 10px 0;
  width: 100%;

  border-width: 1px;
  border-style: solid;
  border-color: var(
    ${props => (props.hasError ? '--border-error' : '--border')}
  );

  color: var(
    ${props => (props.hasError ? '--border-error' : '--primary-text')}
  );

  &::placeholder {
    color: var(${props => (props.hasError ? '--border-error' : '--border')});
  }

  ${props => props.theme.mixins.transition}

  &[type='number']::-webkit-inner-spin-button,
  &[type='number']::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`

interface LabelProps {
  hasError?: boolean
}

const Label = styled.label<LabelProps>`
  display: block;
  position: relative;

  ${props =>
    props.hasError &&
    css`
      &::before {
        position: absolute;
        width: 30%;
        height: 2px;
        background-color: var(--border-error);
        bottom: -2px;
        content: '';
      }
    `}
`

const Box = styled.div`
  width: 100%;
  position: relative;
  margin-top: 15px;
`

const ErrorBox = styled.span`
  font-size: 11px;
  color: var(--border-error);
  position: absolute;
  bottom: -15px;
`

export interface InputProps {
  label?: string
  render?: FunctionComponent
  hasError?: boolean
  error?: FormikErrors<any> | string
}

export const Input = ({
  label,
  render,
  error,
  ...props
}: ReactStyledProps<InputProps>) => {
  const inputField = useRef<HTMLInputElement>(null)

  function handleClickLabel() {
    if (inputField && inputField.current) {
      inputField.current.focus()
    }
  }

  return (
    <Box>
      {label && (
        <Label onClick={handleClickLabel} hasError={render && props.hasError}>
          {label}
        </Label>
      )}
      {!render && <Field ref={inputField} {...props} />}
      {render && render(props, inputField)}
      {props.hasError && <ErrorBox>{error}</ErrorBox>}
    </Box>
  )
}

interface FormInputProps extends FieldProps {
  render?: FunctionComponent
  label?: string
}

export const FormInput = ({
  field,
  form: { touched, errors },
  ...props
}: FormInputProps) => {
  return (
    <Input
      {...field}
      {...props}
      hasError={touched[field.name] && !!errors[field.name]}
      error={errors[field.name]}
      render={props.render}
    />
  )
}
