import styled from 'styled-components'

import { Button } from './button'

interface ButtonGroupProps {
  active?: boolean
}

export const ButtonGroup = styled.div<ButtonGroupProps>`
  width: 100%;
  display: flex;
  padding: 10px 0;

  & > ${Button} {
    flex-grow: 1;
    border-radius: var(--border-radius-box);

    &:not(:last-child) {
      border-right: none;
    }

    &:nth-child(odd) {
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
    }

    &:nth-child(even) {
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
    }
  }
`
