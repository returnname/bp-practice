import styled, { css } from 'styled-components'

interface ButtonProps {
  children: JSX.Element | string
  active?: boolean
  disabled?: boolean
  width?: string
}

const borderVar = (props: ButtonProps) =>
  props.disabled ? '--button-disabled' : '--secondary'

const colorVar = (props: ButtonProps) =>
  props.disabled
    ? '--button-disabled'
    : props.active
    ? '--primary'
    : '--secondary'

export const Button = styled.button<ButtonProps>`
  padding: 12px 10px;
  outline: 0;

  border: 1px solid var(${borderVar});
  border-radius: var(--border-radius-box);

  flex-grow: 1;
  width: ${props => (props.width ? props.width : 'auto')};

  background-color: var(
    ${props => (props.active ? '--secondary' : '--primary')}
  );
  color: var(${colorVar});

  transition: ease-in-out 0.3s;

  ${props =>
    !props.disabled &&
    css`
      &:hover {
        cursor: pointer;
        background-color: var(--secondary);
        color: var(--primary);
      }
    `}
`
