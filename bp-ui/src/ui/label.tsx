import styled from 'styled-components'

export const Label = styled.span`
  padding: 7px 10px;
  background-color: var(--secondary);
  color: var(--secondary-text);
  border-radius: 4px;
`
