import { css } from "styled-components";
import { ThemeInterface } from 'styled-components';

const transition = css`
  transition: ease-in-out 0.2s;
`
const border = css`
  border: 1px solid var(--border);
`

export const theme: ThemeInterface = {
  mixins: {
    border,
    transition
  }
}

export const themeVars = css`
  --primary: #ffffff;
  --primary-text: #24292e;

  --secondary: #00a0ff;
  --secondary-text: #ffffff;

  --button-disabled: #c3c3c3;

  --background: #fafafa;

  --border: #ebeced;
  --border-error: #bf2608;

  --border-radius-box: 4px;
`