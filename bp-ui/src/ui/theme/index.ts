
// tslint:disable: object-literal-sort-keys
export const breakpoints = {
  xs: '480px',
  sm: '768px',
  md: '992px',
  lg: '1200px',
  xl: '1400px' // This one is new
};