import { MixinFunction } from 'styled-components';

export const addBorder: MixinFunction = (props) => props.theme.mixins.border