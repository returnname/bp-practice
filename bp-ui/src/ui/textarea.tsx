import React from 'react'

import { Input, InputProps } from './input'
import { ReactStyledProps } from 'styled-components'

export const Textarea = (props: ReactStyledProps<InputProps>) => (
  <Input {...props} as="textarea" />
)
