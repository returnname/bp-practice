import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from '@apollo/react-hooks'

import { MainLayout } from '../../components/layout/main'
import { Panel } from '../../components/measurment/add-panel'
import { MeasurementList } from './components/measurement-list'
import { Filter } from './components/measurement-filter'
import { authorized } from '../../components/gates/authorized'

const GET_MEASUREMENTS_LIST = gql`
  query GetMeasuremntList {
    measurements {
      dystolic
      systolic
      date
      timeOfDay @client
    }
  }
`

const GET_SORT = gql`
  query GetMeasurementSortList {
    sort: measurementSort @client {
      type
      field
    }
  }
`

const ADD_MEASUREMENT = gql`
  mutation AddMeasurement($measureData: MeasurementInput!) {
    createMeasurement(data: $measureData) {
      user {
        email
      }
    }
  }
`

export const Main = authorized(() => {
  const [visiblePanel, setVisiblePanel] = useState<boolean>(false)

  const { data: listData, loading, refetch } = useQuery(GET_MEASUREMENTS_LIST)
  const { data: sortData } = useQuery(GET_SORT)

  const [createMeasurement] = useMutation(ADD_MEASUREMENT, {
    onCompleted: () => refetch(),
  })

  const right = (
    <Panel
      handleSubmit={async ({ systolic, dystolic, date, comment }) => {
        createMeasurement({
          variables: { measureData: { systolic, dystolic, date, comment } },
        })
      }}
      visible={visiblePanel}
      toggle={() => setVisiblePanel(v => !v)}
    />
  )

  return (
    <MainLayout right={right}>
      <Filter />
      {loading && 'Loading...'}
      {!loading && (
        <MeasurementList list={listData.measurements} sort={sortData.sort} />
      )}
    </MainLayout>
  )
})

export default Main
