import React from 'react'
import { default as ReactSelect } from 'react-select'
import styled from 'styled-components'
import { useMutation } from '@apollo/react-hooks'
import { Box, Flex } from 'reflexbox/styled-components'

import gql from 'graphql-tag'

const Container = styled(Flex)`
  border-bottom: 1px solid var(--border);
`

const Select = styled(ReactSelect)`
  flex-grow: 0.5;
`
const Label = styled.div`
  color: var(--primary-text);
  margin-bottom: 5px;
`

const options = [
  { value: 'ASC', label: 'Newest' },
  { value: 'DESC', label: 'Oldest' },
]

const CHANGE_FILTER = gql`
  mutation ChangeOrderByField($field: String!, $type: String!) {
    changeFilter(field: $field, type: $type) @client
  }
`

export const Filter = () => {
  const [changeFilter] = useMutation(CHANGE_FILTER)
  function handleChange({ value }: any) {
    changeFilter({ variables: { field: 'date', type: value } })
  }

  return (
    <Container pb={[20]}>
      <Box width={[1 / 3]}>
        <Label>Sort:</Label>

        <Select
          placeholder="Order by date"
          name="date"
          options={options}
          onChange={handleChange}
        />
      </Box>
    </Container>
  )
}
