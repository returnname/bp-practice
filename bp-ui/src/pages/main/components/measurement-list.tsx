import React, { FunctionComponent, memo } from 'react'
import { MeasurmentCard } from '../../../components/measurment/card'
import { MeasurementModel, SortList } from '../../../store/types'
import { ItemsList } from '../../../components/items-list'

interface ListProps {
  list: MeasurementModel[]
  sort: SortList<MeasurementModel>
}

export const MeasurementList: FunctionComponent<ListProps> = memo(
  ({ list, sort }) => {
    return (
      <ItemsList
        list={list}
        sort={sort}
        render={(props, key) => <MeasurmentCard key={key} {...props} />}
      />
    )
  }
)
