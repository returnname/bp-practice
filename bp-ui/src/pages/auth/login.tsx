import React from 'react'
import gql from 'graphql-tag'
import { useMutation, useApolloClient } from '@apollo/react-hooks'

import { UnauthorizedLayout } from '../../components/layout/unauthorized'
import { LoginForm } from './components/login-form'
import { Top } from './components/top'
import { redirectIfAuth } from '../../components/gates/redirect-if-auth'
import { useHub } from '../../components/message-hub'

const AUTH_ATTEMPT = gql`
  mutation Login($login: String!, $password: String!) {
    loginUser(email: $login, password: $password) {
      token
    }
  }
`

export const Login = redirectIfAuth(() => {
  const { error } = useHub('login-form')

  const client = useApolloClient()
  const [attemptLogin] = useMutation(AUTH_ATTEMPT, {
    onCompleted: ({ loginUser }) => {
      localStorage.setItem('token', loginUser.token)
      client.writeData({ data: { isLoggedIn: true } })
    },
    onError: ({ graphQLErrors }) => {
      error(graphQLErrors[0].message)
      localStorage.removeItem('token')
      client.writeData({ data: { isLoggedIn: false } })
    },
  })

  const handleLogin = async (creds: { login: string; password: string }) => {
    attemptLogin({ variables: creds })
  }

  return (
    <UnauthorizedLayout top={<Top />}>
      <LoginForm attemptLogin={handleLogin} />
    </UnauthorizedLayout>
  )
})

export default Login
