import React from 'react'
import styled from 'styled-components'

const Logo = styled.h1`
  font-size: 25px;
  color: var(--primary-text);
  font-weight: bold;

  & > span {
    color: #ffb6c1;
  }
`

export const Top = () => (
  <Logo>
    <>Hyper </>
    <span>Nation</span>
  </Logo>
)
