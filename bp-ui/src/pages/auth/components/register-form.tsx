import React from 'react'
import * as Yup from 'yup'

import { Formik, Form, FormikProps, Field, FieldProps } from 'formik'
import { Box } from 'reflexbox/styled-components'
import { ButtonGroup, Button } from '@ui'
import { FormInput } from '@ui/input'

const registerSchema = Yup.object().shape<RegisterValues>({
  email: Yup.string()
    .required('Email required')
    .email('Invalid email'),
  password: Yup.string()
    .required('Password required')
    .min(5),
  confirm_password: Yup.string()
    .required('You must confirm password')
    .oneOf([Yup.ref('password')], 'Passwords must match'),
  firstName: Yup.string().required('First name required'),
  lastName: Yup.string().required('Last name required'),
  role: Yup.string().oneOf(['patient', 'doctor']),
})

const FormTemplate = ({
  values,
  setFieldValue,
  isValid,
}: FormikProps<RegisterValues>) => (
  <>
    <Box padding="20px">
      <Form>
        <Field
          name="email"
          type="text"
          placeholder="Email..."
          label="Email"
          component={FormInput}
        />
        <Field
          name="password"
          type="password"
          placeholder="Password..."
          label="Password"
          component={FormInput}
        />
        <Field
          name="confirm_password"
          type="password"
          placeholder="Password again..."
          label="Confirm password"
          component={FormInput}
        />
        <Field
          name="firstName"
          type="text"
          placeholder="First Name..."
          label="First Name"
          component={FormInput}
        />
        <Field
          name="lastName"
          type="text"
          placeholder="Last Name..."
          label="Last name"
          component={FormInput}
        />
        <Field name="role" type="hidden" />

        <Field
          name="role"
          render={(props: FieldProps) => (
            <FormInput
              {...props}
              label="Role"
              render={() => (
                <ButtonGroup>
                  <Button
                    type="button"
                    active={values.role === 'patient'}
                    onClick={() => setFieldValue('role', 'patient')}
                  >
                    Patient
                  </Button>
                  <Button
                    type="button"
                    active={values.role === 'doctor'}
                    onClick={() => setFieldValue('role', 'doctor')}
                  >
                    Doctor
                  </Button>
                </ButtonGroup>
              )}
            />
          )}
        />

        <Button type="submit" width="100%" disabled={!isValid}>
          Register
        </Button>
      </Form>
    </Box>
  </>
)

interface RegisterValues {
  email: string
  password: string
  confirm_password: string
  firstName: string
  lastName: string
  role: string
}

export interface RegisterForm {
  attemptRegister: (values: RegisterValues) => void
}

export const RegisterForm = ({ attemptRegister }: RegisterForm) => (
  <Formik
    initialValues={{
      email: '',
      password: '',
      confirm_password: '',
      firstName: '',
      lastName: '',
      role: 'patient',
    }}
    validateOnChange={false}
    onSubmit={values => {
      attemptRegister(values)
    }}
    validationSchema={registerSchema}
    render={(props: FormikProps<RegisterValues>) => <FormTemplate {...props} />}
  />
)
