import React from 'react'
import * as Yup from 'yup'

import { Form, FormikProps, Formik } from 'formik'

import { Input, Button } from '@ui'
import { Box, Flex } from 'reflexbox/styled-components'
import { Link } from './login-form-link'
import { Hub as Messages } from '../../../components/form/form-message-hub'

const loginSchema = Yup.object().shape({
  login: Yup.string().email('Invalid email'),
  password: Yup.string().min(5, 'Too Short!'),
})

interface LoginValues {
  login: string
  password: string
}

const FormTemplate = ({
  handleChange,
  handleBlur,
  errors,
}: FormikProps<LoginValues>) => {
  return (
    <>
      <Box padding={[20]}>
        <Messages hubId="login-form" />

        <Form>
          <Input
            name="login"
            type="text"
            placeholder="Login..."
            label="Login"
            onChange={handleChange}
            onBlur={handleBlur}
            hasError={!!errors.login}
          />
          <Input
            name="password"
            type="password"
            placeholder="Password..."
            label="Password"
            onChange={handleChange}
            onBlur={handleBlur}
            hasError={!!errors.password}
          />
          <Box display="flex" width="100%">
            <Button type="submit">Submit</Button>
          </Box>
        </Form>
      </Box>
      <Flex justifyContent="center" p={[10]}>
        <Link to="/register">Register</Link>
        {'/'}
        <Link to="/reset">Reset password</Link>
      </Flex>
    </>
  )
}

interface LoginFormProps {
  attemptLogin: (values: LoginValues) => void
}

export const LoginForm = ({ attemptLogin }: LoginFormProps) => (
  <Formik
    validateOnBlur
    validationSchema={loginSchema}
    initialValues={{ login: '', password: '' }}
    onSubmit={values => attemptLogin(values)}
    render={(props: FormikProps<LoginValues>) => <FormTemplate {...props} />}
  />
)
