import React from 'react'

import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'

import { redirectIfAuth } from '../../components/gates/redirect-if-auth'
import { UnauthorizedLayout } from '../../components/layout/unauthorized'

import { Top } from './components/top'
import { RegisterForm } from './components/register-form'
import { login } from '../../apollo'

const REGISTER_ATTEMPT = gql`
  mutation Register($user: InputRegister!) {
    user: registerUser(user: $user) {
      token
    }
  }
`

interface NewUserInput {
  email: string
  password: string
  firstName: string
  lastName: string
  role: string
}

type HandleRegister = RegisterForm['attemptRegister']

export const Register = redirectIfAuth(() => {
  const [attemptRegister] = useMutation<
    { user: { token: string } },
    { user: NewUserInput }
  >(REGISTER_ATTEMPT, {
    onCompleted: ({ user }) => {
      login(user.token)
    },
  })

  const handleRegister: HandleRegister = ({ confirm_password: _, ...user }) => {
    attemptRegister({
      variables: {
        user,
      },
    })
  }

  return (
    <UnauthorizedLayout top={<Top />}>
      <RegisterForm attemptRegister={handleRegister} />
    </UnauthorizedLayout>
  )
})

export default Register
