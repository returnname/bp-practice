import React from 'react'
import * as Yup from 'yup'
import { Formik, Form as FormContainer, Field } from 'formik'

import { FormInput } from '@ui/input'
import { Button } from '@ui'

import styled from 'styled-components'
import { Hub as FormMessages } from '../../../components/form/form-message-hub'

const Form = styled(FormContainer)`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 10px;
`

const userSchema = Yup.object().shape<Profile>({
  email: Yup.string()
    .required('Email required')
    .email('Invalid email'),
  firstName: Yup.string().required('First name required'),
  lastName: Yup.string().required('Last name required'),
})

const FormTemplate = () => {
  return (
    <Form>
      <FormMessages hubId="profile-form" />

      <Field
        name="email"
        type="text"
        placeholder="Email..."
        label="Email"
        component={FormInput}
      />
      <Field
        name="firstName"
        type="text"
        placeholder="First name..."
        label="First name"
        component={FormInput}
      />
      <Field
        name="lastName"
        type="text"
        placeholder="Last name..."
        label="Last name"
        component={FormInput}
      />
      <Button type="submit"> Update Profile</Button>
    </Form>
  )
}

export interface Profile {
  email: string
  firstName: string
  lastName: string
}

interface ProfileFormProps {
  me: Profile
  updateProfile: (upd: Profile) => void
}

export const ProfileForm = ({ me, updateProfile }: ProfileFormProps) => {
  const { email, firstName, lastName } = me

  return (
    <Formik
      initialValues={{ email, firstName, lastName }}
      validationSchema={userSchema}
      onSubmit={values => {
        updateProfile(values)
      }}
      render={() => <FormTemplate />}
    />
  )
}
