import React from 'react'
import gql from 'graphql-tag'

import { MainLayout } from '../../components/layout/main'
import { authorized } from '../../components/gates/authorized'
import { ProfileForm, Profile as UserProfile } from './components/profile-form'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { useHub } from '../../components/message-hub'

const PROFILE_FIELDS = gql`
  fragment UserProfileFields on User {
    email
    firstName
    lastName
  }
`

const GET_PROFILE = gql`
  query GetProfile {
    me {
      ...UserProfileFields
    }
  }
  ${PROFILE_FIELDS}
`

const UPDATE_PROFILE = gql`
  mutation UpdateProfile($user: InputUpdate!) {
    profile: updateProfile(update: $user) {
      ...UserProfileFields
    }
  }
  ${PROFILE_FIELDS}
`
export const Profile = authorized(() => {
  const { notice } = useHub('profile-form')

  const { data, loading } = useQuery(GET_PROFILE)
  const [updateProfile] = useMutation<
    { profile: UserProfile },
    { user: UserProfile }
  >(UPDATE_PROFILE, {
    update: (cache, { data: updatedData }) => {
      let me = cache.readQuery({ query: GET_PROFILE })

      if (updatedData) {
        me = { ...me, ...updatedData.profile }
      }

      cache.writeQuery({
        query: GET_PROFILE,
        data: { me },
      })
    },
    onCompleted: () => notice('Updated success!'),
  })
  const handleUpdate = (user: UserProfile) => {
    updateProfile({
      variables: {
        user,
      },
    })
  }

  return (
    <MainLayout>
      <>
        <h1>Profile</h1>
        {!loading && <ProfileForm me={data.me} updateProfile={handleUpdate} />}
      </>
    </MainLayout>
  )
})

export default Profile
